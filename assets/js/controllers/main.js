var $ = $,
    Dropzone = Dropzone,
    angular = angular,
    console = console,
    _ = _;

/**
 * @ngdoc function
 * @name fileUploaderAndDownloaderApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the fileUploaderAndDownloaderApp
 */
angular.module('WordCloudGenerator')
.controller('MainCtrl', ['$scope', '$http', function ($scope, $http) {
    var files = [];
    $scope.inputNotClear = false;
    $scope.downloadGenerated = null;
    $('#download').hide();
    
    Dropzone.autoDiscover = false;
    $scope.dropzone = new Dropzone('form#dropzone', {
      paramName: 'file'
    });
    $scope.dropzone.on("addedfile", function(file) {
        files.push(file);
        $scope.readInput();
    });
    
    $scope.readInput = function(){
        $http({method: 'POST', url: '/file/readInput'}).
        success(function(data, status, headers, config) {
            var state = data.status;
            if(state) $scope.inputNotClear = true;
            else $scope.inputNotClear = false;
        }).
        error(function(data, status, headers, config) {
            console.log(data);
        });
    };
    
    $scope.removeFiles = function(){
        $http({method: 'POST', url: '/file/clear'}).
        success(function(data, status, headers, config) {
            _.each(files, function(n,i){
                $scope.dropzone.removeFile(n);
            });
            $scope.inputNotClear = false;
        }).
        error(function(data, status, headers, config) {
            console.log(data);
        });
    };
    
    $scope.convert = function(){
        $('#download').hide();
        $http({method: 'POST', url: '/file/convert'}).
        success(function(data, status, headers, config) {
            var state = data.status;
            if(state) {
                $scope.removeFiles();
                $scope.readInput();
                $scope.downloadGenerated = new Date();
                console.log($scope.downloadGenerated);
                $('#download').show();
            }
        }).
        error(function(data, status, headers, config) {
            console.log(data);
        });
    };
    
    $scope.readInput();
}]);
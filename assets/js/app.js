'use strict';

/**
 * @ngdoc overview
 * @name fileUploaderAndDownloaderApp
 * @description
 * # fileUploaderAndDownloaderApp
 *
 * Main module of the application.
 */
angular
  .module('WordCloudGenerator', [
//    'ngAnimate',
//    'ngCookies',
//    'ngResource',
    'ngRoute'
//    'ngSanitize',
//    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'templates/main.ejs',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

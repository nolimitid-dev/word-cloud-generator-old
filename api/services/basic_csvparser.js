(function(){

	var func = function(str,fieldSelection,filter){
		filter = filter || {};
		var lines = str.split('\n');
		var header = lines.shift();
//        console.log(str);
//        console.log(lines);
		var table = [];
		var headerFieldList = header.split(",");
		var idSelection = null;
		headerFieldList.forEach(function(head,idx){			
			if(head == fieldSelection){
				idSelection = idx;
			}
		})
		//console.log(idSelection);
		lines.forEach(function(line){
			var records = [];
			var recordToggle = '';
			var strLength = line.length;
			var fieldRecord = "";			
			var chr = "";
			//console.log(line);
			var dummyRecord=  "";
			for(var i in line){
				i = parseInt(i,10);
				chr = line[i];
				dummyRecord += chr;
				if(i == strLength - 1){					
					records.push(fieldRecord);
				}else{
					if(recordToggle == '' && chr == "'"){
						recordToggle += "'";
					}else if(chr == "'" && recordToggle == "'" && line[i+1] == "," && line[i+2] == "'"){
						//this is field boundary
						recordToggle += "'";
						records.push(fieldRecord);						
						fieldRecord = "";
					}else if(chr == "," && recordToggle == "''"){
						//this is field delimiter
						recordToggle = "";
					}else{
						fieldRecord += chr;
					}
					
				}
				//console.log(i,i+1,i+2)
				//console.log(chr,line[i+1],line[i+2]);				
				//console.log(recordToggle,chr,line[i+1],line[i+2],fieldRecord,line);
			}
			var fullRecord = {};
			if(headerFieldList.length != records.length){
				console.log(headerFieldList.length,records.length);
			}else{
				headerFieldList.forEach(function(val,i){
					fullRecord[val] = records[i];
				})
			}
			if(filter){
				var include = true;
				for(var i in filter){
					var filterKey = i;
					var filterVal = filter[i];
					if(fullRecord[filterKey]){
						include = include && (fullRecord[filterKey] == filterVal);
					}
				}
				if(!include){
					records = [];
					recordToggle = "";
					chr = "";
					return;
				}
			}

			if(idSelection){
				table.push(records[idSelection]);	
			}else{
				table.push(fullRecord);
			}
			
			records = [];
			recordToggle = "";
			chr = "";
		});
		str = "";
		lines = null;
		return table;
	};

	module.exports = func;
})();
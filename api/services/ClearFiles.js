/* jshint node:true */
var fs = require('fs');
module.exports = function(){
    var path = '.tmp/uploads/input';
    var deleteFolderRecursive = function(path) {
        fs.readdirSync(path).forEach(function(file,index) {
            var curPath = path + "/" + file;
            if(fs.statSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
    };
    deleteFolderRecursive(path);
};
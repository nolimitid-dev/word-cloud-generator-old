(function(){
	var underscore = require('lodash')._;
	var main = function(list,extra){
		var index = {};
		var splitter=  /\s/;
		var regexSearch = null;
//		if(extra.search){
//			regexSearch = new RegExp(extra.search.toLowerCase(),"ig");
//		}
		_.each(list, function(n,i){
            var sentence = n.content || null;
			if(underscore.isString(sentence)){
//            console.log(sentence);
				var pass = true;
				if(regexSearch){
					if(sentence.search(regexSearch) >= 0){
						pass = true;
					}else{
						pass = false;
					}
				}
				if(pass){
					var words = sentence.split(splitter);
					words.forEach(function(word){
						//word = word.toLowerCase();
						if(!index[word]){
							index[word] = {
								content:word,
								count:0
							};
						}
						index[word].count += 1;
					});
					delete words;
						
				}
				delete sentence;
			}
			
			//console.log(sentence.split(/\s/))
			
		});
		delete list;
		return underscore.sortBy(underscore.values(index),function(entry){
			return 0-entry.count;
		});
	};

	module.exports = main;
})();
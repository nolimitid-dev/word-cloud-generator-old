/* jshint node:true */
/**
 * FileController
 *
 * @description :: Server-side logic for managing files
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var fs = require('fs'),
    ClearFiles = require('../services/ClearFiles.js'),
    _ = require('lodash'),
    csv = require('fast-csv'),
    parser = require('../services/basic_csvparser'),
    clouder = require('../services/clouder'),
    Promise = require('when/es6-shim/Promise');

module.exports = {
	upload: function(req, res){
        if(req.method !== 'POST') res.json({'status':'Must POST'});
        req.file('file')
        .upload({
            dirname: 'input'
        },function(err, file){
            if(err) res.json(err);
            res.json(file);
        });
    },
    convert: function(req, res){
        var path = '.tmp/uploads/input',
            arr = fs.readdirSync(path),
            promises;
        
        promises = _.map(arr, function(n){
            return new Promise(function(resolve, reject){
                var datas = [],
                    wordCloud,
                    stream = fs.createReadStream(path+'/'+n);

                csv
                .fromStream(stream, {headers: true})
                .on('data', function(data){
                    datas = datas.concat(data);
                })
                .on('end', function(){
                    resolve(datas);
                });
            });
        });
        
        Promise.all(promises)
        .then(function(datas){
            var input = [],
                wordCloud;
            _.each(datas, function(n,i){
                input = input.concat(n);
            });
            wordCloud = clouder(input);
            var datasFix = _.map(wordCloud, function(n){
                return {
                    key: n.content,
                    value: n.count
                };
            });
            var ws = fs.createWriteStream('.tmp/public/cloud.csv');

            csv
            .write(datasFix, {headers: true})
            .pipe(ws);

            ws.on('finish', function(){
                ClearFiles();
                res.json({
                    status: true
                });
            });
        });
    },
    clear: function(req, res){
        ClearFiles();
        res.json({
            status: true
        });
    },
    readInput: function(req,res){
        var path = '.tmp/uploads/input',
            arr = fs.readdirSync(path);
        if(arr.length > 0) {
            res.json({
                status: true,
                data: arr
            });
        }
        else {
            res.json({
                status: false
            });
        }
    }
};
